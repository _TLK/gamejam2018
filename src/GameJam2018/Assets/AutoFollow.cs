﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AutoFollow : MonoBehaviour 
{
  private int _destPoint;
  private NavMeshAgent _agent;
  
  List<Character> _characters;

  void Start ()
  {
    _characters = Registry.Characters;
    
    _agent = GetComponent<NavMeshAgent>();
    _agent.autoBraking = false;
  }
  
  
  void Update()
  {
    foreach (Character character in _characters)
    {
      if (character.gameObject.activeInHierarchy)
      {
        _agent.destination = character.transform.position;
      }
    }
  }
}
