// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Flor"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
		_Color0("Color 0", Color) = (1,1,1,0)
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Lerp("Lerp", Range( 0 , 1)) = 1
		_Remap("Remap", Range( 0 , 1)) = 1
		_Tex("Tex", Range( 0 , 1)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform float _Tex;
		uniform float _Remap;
		uniform float _Lerp;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 temp_output_5_0 = ( tex2D( _TextureSample0, uv_TextureSample0 ) * _Tex );
			float4 _Vector0 = float4(0,1,-0.4,1.5);
			float4 temp_cast_1 = (_Vector0.x).xxxx;
			float4 temp_cast_2 = (_Vector0.y).xxxx;
			float4 temp_cast_3 = (_Vector0.z).xxxx;
			float4 temp_cast_4 = (_Vector0.w).xxxx;
			o.Albedo = saturate( lerp( ( _Color0 * temp_output_5_0 ) , ( (temp_cast_3 + (temp_output_5_0 - temp_cast_1) * (temp_cast_4 - temp_cast_3) / (temp_cast_2 - temp_cast_1)) * _Remap ) , _Lerp ) ).xyz;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=10001
832;1069;1752;805;-8.900351;-212.0999;1.3;True;False
Node;AmplifyShaderEditor.SamplerNode;2;-191.5,207.8;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;0.0;False;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;4;-129.5999,424.1001;Float;False;Property;_Tex;Tex;4;0;1;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;391.5002,219.7;Float;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.Vector4Node;7;341.8999,453.2001;Float;False;Constant;_Vector0;Vector 0;3;0;0,1,-0.4,1.5;0;5;FLOAT4;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;9;528.0999,765.2997;Float;False;Property;_Remap;Remap;3;0;1;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.TFHCRemap;6;688.8998,423.5999;Float;False;5;0;FLOAT4;0.0;False;1;FLOAT4;0.0;False;2;FLOAT4;1.0;False;3;FLOAT4;0.0;False;4;FLOAT4;1.0;False;1;FLOAT4
Node;AmplifyShaderEditor.ColorNode;1;200.8,-27.59999;Float;False;Property;_Color0;Color 0;0;0;1,1,1,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;959.4008,449.0996;Float;False;2;0;FLOAT4;0.0;False;1;FLOAT;0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.RangedFloatNode;13;954.8007,865.4002;Float;False;Property;_Lerp;Lerp;2;0;1;0;1;0;1;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;701.1,166.9;Float;False;2;0;COLOR;0.0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4
Node;AmplifyShaderEditor.LerpOp;12;1321.6,211.2;Float;False;3;0;FLOAT4;0.0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.SaturateNode;14;1626.9,218.8998;Float;False;1;0;FLOAT4;0.0;False;1;FLOAT4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1900.001,208.3999;Float;False;True;2;Float;ASEMaterialInspector;0;Standard;Flor;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;False;0;4;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;Add;Add;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;Relative;0;;-1;-1;-1;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;OBJECT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;13;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;5;0;2;0
WireConnection;5;1;4;0
WireConnection;6;0;5;0
WireConnection;6;1;7;1
WireConnection;6;2;7;2
WireConnection;6;3;7;3
WireConnection;6;4;7;4
WireConnection;8;0;6;0
WireConnection;8;1;9;0
WireConnection;3;0;1;0
WireConnection;3;1;5;0
WireConnection;12;0;3;0
WireConnection;12;1;8;0
WireConnection;12;2;13;0
WireConnection;14;0;12;0
WireConnection;0;0;14;0
ASEEND*/
//CHKSM=5FBABD6019D151F46CCBDA6C9A2A3A5690C09338