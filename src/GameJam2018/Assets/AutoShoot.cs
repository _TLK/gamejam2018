﻿using UnityEngine;

public class AutoShoot : MonoBehaviour {

  public GameObject BulletPrefab;
  public float InitialDistance = 1;
  public float DestroyAfterSeconds = 5;
  public float DelayBetweenShots = 1;
  public Vector3 DeltaTransform = new Vector3(0, 0.5f, 0);

  float _cooldown = 0;

  void Update()
  {
    _cooldown += Time.deltaTime;

    if (_cooldown > DelayBetweenShots)
    {
      _cooldown = 0;

      GetComponent<Animator>().SetTrigger("Attack");
      
      // Create the Bullet from the Bullet Prefab
      var bullet = (GameObject)Instantiate(
        BulletPrefab,
        DeltaTransform + this.transform.position + this.transform.forward.normalized * InitialDistance,
        this.transform.rotation);

      // Add velocity to the bullet
      //bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

      // Destroy the bullet after 2 seconds
      Destroy(bullet, DestroyAfterSeconds);
    }
  }
}
