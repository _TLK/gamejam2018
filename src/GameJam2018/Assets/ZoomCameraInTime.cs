﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomCameraInTime : MonoBehaviour
{
  public float From = 2;
  public float To = -5.5f;
  public float Seconds = 5;

  float _timePassed;
  Camera _camera;

  void Update()
  {
    _timePassed += Time.deltaTime;
    float progress = _timePassed / Seconds;
    if (progress > 1)
      progress = 1;

    var camera = _camera ?? (_camera = this.GetComponent<Camera> ());
    camera.orthographicSize =
      (1 - progress) * From + progress * To;
  }
}
