﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformToDefaultInTime : MonoBehaviour
{
  public float Seconds = 5;

  float _timePassed;
  Vector3 _initialPosition;
  Vector3 _initialRotationAngles;
  Vector3 _initialScale;

  void Start ()
  {
    _initialPosition = this.transform.position;
    _initialRotationAngles = this.transform.rotation.eulerAngles;
    _initialScale = this.transform.localScale;
  }

	void Update ()
	{
	  _timePassed += Time.deltaTime;
	  float progress = _timePassed / Seconds;
	  if (progress > 1)
	    progress = 1;

	  float progressToZero = 1 - progress;
	  float progressToOne = progress;

	  this.transform.position = progressToZero * _initialPosition;
    this.transform.rotation = Quaternion.Euler(
      new Vector3(
        progressToZero * _initialRotationAngles.x,
        progressToZero * _initialRotationAngles.y,
        progressToZero * _initialRotationAngles.z));
	  this.transform.localScale =
	    progressToOne * new Vector3 (1, 1, 1) +
	    progressToZero * _initialScale;
	}
}
