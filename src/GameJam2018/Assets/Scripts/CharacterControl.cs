﻿using UnityEngine;

public class CharacterControl : MonoBehaviour
{
  public float DashSpeed = 50.0F;
  public float DashLengthInSeconds = 0.1F;
  public float DashCooldownInSeconds = 2;
  public float Speed = 6.0F;
  public float JumpSpeed = 8.0F;
  public float Gravity = 20.0F;

  public bool Rotate = true;
  public bool AlwaysJump = false;
  public bool Invert = false;


  public AudioClip FootstepSound;

  Vector3 moveDirection = Vector3.zero;
  CharacterController _controller;

  private AudioSource _audioSource;

  bool _dash;
  float _dashTime = 0;
  float _dashCooldownTime = 0;
  Vector3 _dashDirection;
  
  private Animator _animator;

  private void Start()
  {
    _audioSource = GetComponent<AudioSource>();
    _animator = GetComponent<Animator>();
  }

  void Update()
  {
    CharacterController controller = _controller ?? (_controller = GetComponent<CharacterController>());

    Vector3? lookDirection = null;

    if (controller.isGrounded)
    {
      moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

      if (moveDirection != Vector3.zero)
      {
        lookDirection = moveDirection;
        var magnitude = moveDirection.magnitude;
        PlayFootstep(FootstepSound, magnitude);
        if (_animator != null)
          _animator.SetFloat("Speed", magnitude);
      }
      else
      {
        StopFootstep();
      }

      //moveDirection = transform.TransformDirection(moveDirection);
      moveDirection *= Speed;
      if (AlwaysJump)
        moveDirection.y = JumpSpeed;
    }

    if (lookDirection.HasValue && Rotate)
    {
      this.transform.LookAt (this.transform.position + lookDirection.Value);
    }

    _dashCooldownTime += Time.deltaTime;

    if (!_dash && Input.GetButtonDown ("Jump") && _dashCooldownTime >= DashCooldownInSeconds)
    {
      _dash = true;
      _dashDirection = new Vector3(this.transform.forward.x, 0, this.transform.forward.z).normalized;
      _dashDirection *= DashSpeed;
      _dashTime = 0;
    }

    moveDirection = this.InvertMoveIfNeeded (moveDirection);

    if (_dash)
    {
      moveDirection += _dashDirection;

      _dashTime += Time.deltaTime;

      if (_dashTime >= DashLengthInSeconds)
      {
        _dash = false;
        _dashCooldownTime = 0;
      }
    }

    moveDirection.y -= Gravity * Time.deltaTime;
    controller.Move(moveDirection * Time.deltaTime);
  }

  private void PlayFootstep (AudioClip stepSound, float speed)
  {
    if (_audioSource.isPlaying)
      return;
    
    _audioSource.clip = stepSound;
    _audioSource.volume = Random.Range (0.5f, 1.0f);
    _audioSource.pitch = 0.7f + speed;
    _audioSource.PlayOneShot(stepSound);
  }

  private void StopFootstep()
  {
    _audioSource.Stop();
  }
  Vector3 InvertMoveIfNeeded (Vector3 move)
  {
    return new Vector3(
      Invert ? -move.x : move.x,
      move.y,
      Invert ? -move.z : move.z
      );
  }
}
