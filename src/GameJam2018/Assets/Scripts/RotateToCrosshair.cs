﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToCrosshair : MonoBehaviour
{
  public Transform Crosshair;

	void Update ()
  {
    if (Crosshair != null)
    {
      this.transform.LookAt(
        new Vector3(
          this.Crosshair.position.x,
          this.transform.position.y,
          this.Crosshair.position.z));
    }
  }
}
