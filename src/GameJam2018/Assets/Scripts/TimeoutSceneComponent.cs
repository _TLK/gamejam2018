﻿using System.Collections;
using UnityEngine;

public class TimeoutSceneComponent : MonoBehaviour
{
   public int SecondsToTimeout;
   
   private void Start()
   {
      StartCoroutine(TimeoutRoutine(SecondsToTimeout));
   }

   private IEnumerator TimeoutRoutine(int timeoutInSecs)
   {
      yield return new WaitForSeconds(timeoutInSecs);
      
      GetComponent<NextSceneLoader>().LoadNextScene();
   }
}
