﻿using UnityEngine;

public class Character : MonoBehaviour
{
  public bool Imaginary;

  void Awake ()
  {
    Registry.Add (this);
  }

  void Start ()
  {
    if (Imaginary)
      this.gameObject.SetActive (false);
  }

  void OnDestroy ()
  {
    Registry.Remove (this);
  }
}