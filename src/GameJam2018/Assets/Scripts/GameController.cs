﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : SceneController
{
    private string _currentGameLevel;
    
    private void Start()
    {
        GetComponent<SceneLoadingManager>().LoadScene("Title");
        
        SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    
    public void Dream()
    {
        FindObjectOfType<SceneLoadingManager>().LoadSceneAndUnloadAllPrevious("BonusLevel");
    }

    public void RetryLevel()
    {
        if (!string.IsNullOrEmpty(_currentGameLevel))
        {
            FindObjectOfType<SceneLoadingManager>().LoadSceneAndUnloadAllPrevious(_currentGameLevel);
        }
    }

    public void Die()
    {
        FindObjectOfType<SceneLoadingManager>().LoadScene("YouDied");
    }
    
    private void SceneManagerOnSceneLoaded(Scene loadedScene, LoadSceneMode mode)
    {
        var rootGameObjects = loadedScene.GetRootGameObjects();
        foreach (var rootGameObject in rootGameObjects)
        {
            var levelController = rootGameObject.GetComponentInChildren<GameLevelController>();
            if (levelController != null)
            {
                _currentGameLevel = loadedScene.name;
                break;
            }
        }
    }
}