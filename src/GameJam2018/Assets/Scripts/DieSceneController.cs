﻿using UnityEngine;

public class DieSceneController : MonoBehaviour 
{
    public void Retry()
    {
       FindObjectOfType<GameController>().RetryLevel();
    }

    public void Exit()
    {
        FindObjectOfType<GameController>().QuitGame();
    }

    public void Dream()
    {
        FindObjectOfType<GameController>().Dream();
    }
}