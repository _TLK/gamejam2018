﻿using UnityEngine;

public class StopMainMusicOnUnload : MonoBehaviour
{
  //private AudioSource _sourceToStop;
  //private void Start()
  //{
  //  var gameObject = GameObject.FindGameObjectWithTag("MainSceneAudioSource");
  //  if (gameObject != null)
  //  {
  //    print("FUCK!!!");
  //    var _sourceToStop = gameObject.GetComponent<AudioSource>();
  //  }
  //}

  public void Stop()
  {
    Debug.Log ("Try Stop");
    var audioSource = Registry.MainMusic.GetComponent<AudioSource>();
    if (audioSource != null)
    {
      audioSource.Stop();
      //Destroy(_sourceToStop.gameObject);
    }
    var audioListener = Registry.MainMusic.GetComponent<AudioListener>();
    if (audioListener != null)
    {
      Destroy(audioListener);
      //Destroy(_sourceToStop.gameObject);
    }
  }
}
