﻿using UnityEngine;

public class DisappearOnZeroHealth : MonoBehaviour
{
  Health _health;

  void Update()
  {
    var health = _health ?? (_health = this.GetComponent<Health>());

    if (health == null) return;

    if (health.CurrentValue <= 0)
    {
      var simpleEnemy = GetComponent<SimpleEnemy>();
      if(simpleEnemy != null)
        simpleEnemy.NotifyOnDeath();
      
      Destroy (this.gameObject);
    }
  }
}