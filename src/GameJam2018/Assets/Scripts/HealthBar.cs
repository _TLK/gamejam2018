﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
  public Health _healthComponentToFollow;

  private Slider _slider;

  private void Awake()
  {
    _slider = GetComponent<Slider>();
    
    if(_healthComponentToFollow == null)
      Debug.LogErrorFormat("There is no Health component to follow in {0}", name);
  }

  private void LateUpdate()
  {
    if (_healthComponentToFollow != null && _slider != null)
      _slider.value = _healthComponentToFollow.CurrentValue / _healthComponentToFollow.MaxValue;

  }
}
