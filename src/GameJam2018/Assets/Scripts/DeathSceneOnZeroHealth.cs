﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class DeathSceneOnZeroHealth : MonoBehaviour
{
  Health _health;
  private Animator _animator;

  private List<Type> _typesToDestroyOnDeath = new List<Type>{typeof(CharacterControl), typeof(SwitchCharacterOnAltFire), typeof(FireBullets) };

  private void Start()
  {
    _animator = GetComponent<Animator>();
  }
  
  void Update()
  {
    var health = _health ?? (_health = this.GetComponent<Health>());

    if (health == null) return;

    if (health.CurrentValue <= 0)
    {
      _animator.SetTrigger("Die");

      foreach (var type in _typesToDestroyOnDeath)
      {
        var control = transform.GetComponentInChildren(type);
        if(control)
          Destroy(control);
      }
      
      FindObjectOfType<GameController>().Die();

      var rotateToCrosshair = this.GetComponent<RotateToCrosshair> ();
      if (rotateToCrosshair != null)
        rotateToCrosshair.enabled = false;

      Destroy(this);
    }
  }
}