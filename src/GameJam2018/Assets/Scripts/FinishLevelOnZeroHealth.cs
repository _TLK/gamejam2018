﻿using UnityEngine;

public class FinishLevelOnZeroHealth : MonoBehaviour
{
  Health _health;

  void Update()
  {
    var health = _health ?? (_health = this.GetComponent<Health>());

    if (health == null) return;

    if (health.CurrentValue <= 0)
    {
      FindObjectOfType<NextSceneLoader>().LoadNextScene();
    }
  }
}