﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
  public float MaxValue = 100;
  public float CurrentValue;

  private void Awake()
  {
    CurrentValue = MaxValue;
  }
}
