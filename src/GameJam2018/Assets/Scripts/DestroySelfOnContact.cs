﻿using UnityEngine;

public class DestroySelfOnContact : MonoBehaviour
{
  bool _shouldDestroy;

  void OnCollisionEnter(Collision other)
  {
    this.DestroySelf();
  }

  void OnTriggerEnter(Collider other)
  {
    this.DestroySelf();
  }

  void DestroySelf()
  {
    _shouldDestroy = true;
  }

  void LateUpdate ()
  {
    if (_shouldDestroy)
      Destroy (this.gameObject);
  }
}