﻿using UnityEngine;

public class DecreaseHealthOnContact : MonoBehaviour
{
  public float Value = 13;

  void OnCollisionEnter(Collision other)
  {
    this.DecreaseHealth(other.gameObject);
  }

  void OnTriggerEnter(Collider other)
  {
    this.DecreaseHealth(other.gameObject);
  }

  void DecreaseHealth(GameObject other)
  {
    var health = other.GetComponent<Health>();
    if (health == null) return;

    health.CurrentValue -= Value;
  }
}