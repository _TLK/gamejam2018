﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(PatrolComponent))]
public class StopPatrolNearCharacter : MonoBehaviour
{
  public float Distance = 7;

  List<Character> _characters;

  void Start ()
  {
    _characters = Registry.Characters;
  }

  void Update ()
  {
    foreach (Character character in _characters)
    {
      if (character.gameObject.activeInHierarchy)
      {
        float distance = Vector3.Distance (this.transform.position, character.transform.position);

        if (distance < Distance)
        {
          this.Stop ();
        }
      }
    }
  }

  void Stop ()
  {
    var patrol = this.GetComponent<PatrolComponent> ();
    patrol.enabled = false;

    var navMeshAgent = this.GetComponent<NavMeshAgent> ();
    navMeshAgent.isStopped = true;
  }
}