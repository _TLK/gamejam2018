﻿using System.Runtime.Serialization.Formatters;
using UnityEngine;

[RequireComponent(typeof(DisappearOnZeroHealth))]
public class SimpleEnemy : MonoBehaviour
{
    private BossRevealOnSimpleEnemiesDeath _boss;

    private void Start()
    {
        _boss = FindObjectOfType<BossRevealOnSimpleEnemiesDeath>();
    }

    public void NotifyOnDeath()
    {
        if (_boss != null)
        {
            _boss.NotifyOnSimpleDeath(this);
        }
    }
}
