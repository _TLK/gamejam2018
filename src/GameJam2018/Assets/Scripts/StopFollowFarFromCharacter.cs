﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AutoFollow))]
[RequireComponent(typeof(RotateToCrosshair))]
public class StopFollowFarFromCharacter : MonoBehaviour
{
  public float Distance = 15;

  List<Character> _characters;

  void Start ()
  {
    _characters = Registry.Characters;
  }

  void Update ()
  {
    foreach (Character character in _characters)
    {
      if (character.gameObject.activeInHierarchy)
      {
        float distance = Vector3.Distance (this.transform.position, character.transform.position);

        if (distance > Distance)
        {
          this.StopAim (character);
        }
      }
    }
  }

  void StopAim(Character character)
  {
    var aim = this.GetComponent<RotateToCrosshair>();
    aim.Crosshair = character.transform;
    aim.enabled = false;

    var follow = this.GetComponent<AutoFollow>();
    follow.enabled = false;
  }
}