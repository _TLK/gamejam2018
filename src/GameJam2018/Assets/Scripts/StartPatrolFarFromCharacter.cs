﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(PatrolComponent))]
public class StartPatrolFarFromCharacter : MonoBehaviour
{
  public float Distance = 15;

  List<Character> _characters;

  void Start ()
  {
    _characters = Registry.Characters;
  }

  void Update ()
  {
    foreach (Character character in _characters)
    {
      if (character.gameObject.activeInHierarchy)
      {
        float distance = Vector3.Distance (this.transform.position, character.transform.position);

        if (distance > Distance)
        {
          this.Patrol ();
        }
      }
    }
  }

  void Patrol ()
  {
    var patrol = this.GetComponent<PatrolComponent> ();
    patrol.enabled = true;

    var navMeshAgent = this.GetComponent<NavMeshAgent> ();
    navMeshAgent.isStopped = false;
  }
}