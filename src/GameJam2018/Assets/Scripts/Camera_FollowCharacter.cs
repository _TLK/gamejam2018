﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_FollowCharacter : MonoBehaviour
{
  public Transform Character;
  public Transform SecondaryCharacter;
  public float Factor = 0.1f;
  public float MetersInFront = 2;

  Vector3 verticalOffset;

  void Start ()
  {
    verticalOffset = Vector3.up * (this.transform.position.y - Character.position.y);
  }

  void LateUpdate ()
  {
    if (Character == null)
      Character = SecondaryCharacter;

    if (!Character.gameObject.activeInHierarchy && SecondaryCharacter != null)
    {
      var temp = Character;
      Character = SecondaryCharacter;
      SecondaryCharacter = temp;
    }

    Vector3 targetPosition = Character.position + verticalOffset + MetersInFront * Character.forward.normalized;

    Vector3 newPosition =
      (1 - Factor) * this.transform.position +
      Factor * targetPosition;

    this.transform.position = newPosition;
  }
}
