﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AutoShoot))]
[RequireComponent(typeof(RotateToCrosshair))]
public class StartAimNearCharacter : MonoBehaviour
{
  public float Distance = 7;

  List<Character> _characters;

  void Start ()
  {
    _characters = Registry.Characters;
  }

  void Update ()
  {
    foreach (Character character in _characters)
    {
      if (character.gameObject.activeInHierarchy)
      {
        float distance = Vector3.Distance (this.transform.position, character.transform.position);

        if (distance < Distance)
        {
          this.StartAim (character);
        }
      }
    }
  }

  void StartAim (Character character)
  {
    var aim = this.GetComponent<RotateToCrosshair> ();
    aim.Crosshair = character.transform;
    aim.enabled = true;

    var shoot = this.GetComponent<AutoShoot>();
    shoot.enabled = true;
  }
}