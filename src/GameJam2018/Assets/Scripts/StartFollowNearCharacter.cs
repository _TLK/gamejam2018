﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AutoFollow))]
[RequireComponent(typeof(RotateToCrosshair))]
public class StartFollowNearCharacter : MonoBehaviour
{
  public float Distance = 7;

  List<Character> _characters;

  void Start ()
  {
    _characters = Registry.Characters;
  }

  void Update ()
  {
    foreach (Character character in _characters)
    {
      if (character.gameObject.activeInHierarchy)
      {
        float distance = Vector3.Distance (this.transform.position, character.transform.position);

        if (distance < Distance)
        {
          this.StartFollow (character);
        }
      }
    }
  }

  void StartFollow (Character character)
  {
    var aim = this.GetComponent<RotateToCrosshair> ();
    aim.Crosshair = character.transform;
    aim.enabled = true;

    var follow = this.GetComponent<AutoFollow>();
    follow.enabled = true;
  }
}