﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouseCursor : MonoBehaviour
{
  void Update()
  {
    var cursorPosition = Camera.main.ScreenToWorldPoint(
      new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y));
    var targetPosition = new Vector3(cursorPosition.x, this.transform.position.y, cursorPosition.z);
    this.transform.position = targetPosition;
  }
}
