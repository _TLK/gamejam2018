﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BossRevealOnSimpleEnemiesDeath : MonoBehaviour
{
  public float ShowBossSeconds = 7;
  public GameObject[] ObjectsToActivate;  
  
  private List<Character> _characters;

  private List<SimpleEnemy> _simpleEnemies;
  Camera_FollowCharacter _camera;

  private const int DefaultLayer = 0;
    private void Start()
    {
        _simpleEnemies = FindObjectsOfType<SimpleEnemy>().ToList();
        _characters = Registry.Characters;
      _camera = FindObjectOfType<Camera_FollowCharacter> ();
    }

  public void NotifyOnSimpleDeath(SimpleEnemy enemy)
    {
        _simpleEnemies.Remove(enemy);
        if (_simpleEnemies.Count == 0)
        {
            RevealSelf();
            ActivateBehaviour();
            MergeGirls ();

          StartCoroutine(FollowBoss ());
        }
    }

  IEnumerator FollowBoss ()
  {
    var initialChar = _camera.Character;
    initialChar.gameObject.SetActive(false);
    _camera.Character = this.gameObject.transform;
    yield return new WaitForSeconds (ShowBossSeconds);

    foreach (Character character in _characters)
    {
      character.gameObject.SetActive (!character.Imaginary);

      if (character.Imaginary)
        _camera.SecondaryCharacter = character.transform;
      else
        _camera.Character = character.transform;
    }
  }

  void MergeGirls ()
  {
    foreach (Character character in _characters)
    {
      var switchCharacterOnAltFire = character.gameObject.GetComponent<SwitchCharacterOnAltFire> ();
      if (switchCharacterOnAltFire != null)
      {
        if (character.Imaginary)
          switchCharacterOnAltFire.Switch ();
      }
    }

    foreach (Character character in _characters)
    {
      var switchCharacterOnAltFire = character.gameObject.GetComponent<SwitchCharacterOnAltFire> ();
      if (switchCharacterOnAltFire != null)
      {
        switchCharacterOnAltFire.enabled = false;
      }

      if (character.Imaginary)
      {
        var control = character.gameObject.GetComponent<CharacterControl>();
        if (control != null)
          control.Invert = true;
      }

      var spawnImaginaryCharacter = character.gameObject.GetComponent<SpawnImaginaryCharacterOnAltFire> ();
      if (spawnImaginaryCharacter != null)
        spawnImaginaryCharacter.enabled = true;

      var correctPositionByBossAndBuddy = character.gameObject.GetComponent<CorrectPositionByBossAndBuddy> ();
      if (correctPositionByBossAndBuddy != null)
        correctPositionByBossAndBuddy.enabled = true;
    }
  }

  private void RevealSelf()
    {
        gameObject.layer = DefaultLayer;
        foreach (var trans in gameObject.GetComponentsInChildren<Transform>(true))
        {
            trans.gameObject.layer = DefaultLayer;
        }
    }

    private void ActivateBehaviour()
    {
        GetComponent<PatrolComponent>().enabled = true;

        foreach (var gameObject in ObjectsToActivate)
        {
            gameObject.SetActive(true);
        }
    }
}
