﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadingManager : MonoBehaviour
{
   private readonly List<string> _currentSceneNames = new List<string>();

   public void LoadSceneAndUnloadAllPrevious(string sceneName)
   {
      foreach (var currentSceneName in _currentSceneNames)
      {
         if (!string.IsNullOrEmpty(currentSceneName))
            SceneManager.UnloadSceneAsync(currentSceneName);
      }
      _currentSceneNames.Clear();

      LoadScene(sceneName);
   }
   
   public void LoadScene(string sceneName)
   {
      _currentSceneNames.Add(sceneName);
      SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
   }
}
