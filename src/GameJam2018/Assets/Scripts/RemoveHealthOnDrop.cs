﻿using UnityEngine;

public class RemoveHealthOnDrop : MonoBehaviour
{
  public float DropLimit = -5;

  void Update ()
  {
    if (this.transform.position.y < DropLimit)
    {
      var health = this.GetComponent<Health>();
      if (health == null) return;

      health.CurrentValue = -1;
    }
  }
}