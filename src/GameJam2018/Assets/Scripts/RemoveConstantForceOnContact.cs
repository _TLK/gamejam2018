﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveConstantForceOnContact : MonoBehaviour
{
  void OnCollisionEnter (Collision other)
  {
    this.RemoveConstantForce ();
  }

  void OnTriggerEnter (Collider other)
  {
    this.RemoveConstantForce();
  }

  void RemoveConstantForce ()
  {
    var force = this.GetComponent<ConstantForce> ();
    if (force != null)
      Destroy (force);
  }
}