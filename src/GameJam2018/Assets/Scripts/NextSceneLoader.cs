﻿using UnityEngine;

public class NextSceneLoader : MonoBehaviour
{
   public string NextSceneName;

   private StopMainMusicOnUnload _optionalStopMusic;

   private void Start()
   {
      _optionalStopMusic = GetComponent<StopMainMusicOnUnload>();
   }
   
   public void LoadNextScene()
   {
      if (string.IsNullOrEmpty(NextSceneName))
         Debug.LogError("None next scene name!");

      if(_optionalStopMusic != null)
         _optionalStopMusic.Stop();
      
      FindObjectOfType<SceneLoadingManager>().LoadSceneAndUnloadAllPrevious(NextSceneName);
   }
}
