﻿using System.Collections.Generic;

public static class Registry
{
  public static readonly List<Character> Characters = new List<Character> ();
  public static MainMusic MainMusic;

  public static void Add (Character character)
  {
    Characters.Add (character);
  }

  public static void Remove (Character character)
  {
    Characters.Remove (character);
  }

  public static void Register (MainMusic mainMusic)
  {
    MainMusic = mainMusic;
  }
}