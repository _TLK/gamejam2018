﻿using UnityEngine;

public class StopRigidbodyOnContact : MonoBehaviour
{
  void OnCollisionEnter (Collision other)
  {
    this.StopRigidbody ();
  }

  void OnTriggerEnter (Collider other)
  {
    this.StopRigidbody();
  }

  void StopRigidbody ()
  {
    var body = this.GetComponent<Rigidbody> ();
    if (body != null)
      body.velocity = Vector3.zero;;
  }
}