﻿using UnityEngine;

public class FireBullets : MonoBehaviour
{
  public GameObject StartFX;
  
  public GameObject BulletPrefab;
  public float InitialDistance = 1;
  public float DestroyAfterSeconds = 5;
  public Vector3 DeltaTransform = new Vector3(0, 0.5f, 0);

  void Update()
  {
    if (Input.GetButtonDown("Fire1"))
    {
      if (StartFX != null)
      {
        var startFX = Instantiate(
          StartFX,
          DeltaTransform + this.transform.position + this.transform.forward.normalized * InitialDistance,
          this.transform.rotation);
      }
      
      // Create the Bullet from the Bullet Prefab
      var bullet = (GameObject)Instantiate(
        BulletPrefab,
        DeltaTransform + this.transform.position + this.transform.forward.normalized * InitialDistance,
        this.transform.rotation);

      // Add velocity to the bullet
      //bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

      // Destroy the bullet after 2 seconds
      Destroy(bullet, DestroyAfterSeconds);
    }
  }
}
