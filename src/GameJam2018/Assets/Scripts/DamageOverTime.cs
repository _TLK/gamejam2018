﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOverTime : MonoBehaviour
{
  public float DPS = 10;

  Health _health;

  void Update ()
  {
    var health = _health ?? (_health = this.GetComponent<Health> ());

    if (health == null) return;

    health.CurrentValue -= Time.deltaTime * DPS;
  }
}