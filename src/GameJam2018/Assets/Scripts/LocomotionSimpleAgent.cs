﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent (typeof (NavMeshAgent))]
[RequireComponent (typeof (Animator))]
public class LocomotionSimpleAgent : MonoBehaviour
{
   private Animator _animator;
   private NavMeshAgent _navMeshAgent;
   private Vector2 _smoothDeltaPosition = Vector2.zero;
   private Vector2 _velocity = Vector2.zero;

   private void Start ()
   {
      _animator = GetComponent<Animator> ();
      _navMeshAgent = GetComponent<NavMeshAgent> ();
      // Don’t update position automatically
      _navMeshAgent.updatePosition = false;
   }

   private void Update ()
   {
//      Vector3 worldDeltaPosition = _navMeshAgent.nextPosition - transform.position;
//
//      // Map 'worldDeltaPosition' to local space
//      float dx = Vector3.Dot (transform.right, worldDeltaPosition);
//      float dy = Vector3.Dot (transform.forward, worldDeltaPosition);
//      Vector2 deltaPosition = new Vector2 (dx, dy);
//
//      // Low-pass filter the deltaMove
//      float smooth = Mathf.Min(1.0f, Time.deltaTime/0.15f);
//      _smoothDeltaPosition = Vector2.Lerp (_smoothDeltaPosition, deltaPosition, smooth);
//
//      // Update velocity if time advances
//      if (Time.deltaTime > 1e-5f)
//         _velocity = _smoothDeltaPosition / Time.deltaTime;
//
//      bool shouldMove = _velocity.magnitude > 0.5f && _navMeshAgent.remainingDistance > _navMeshAgent.radius;
//

      _animator.SetFloat("Speed", _navMeshAgent.velocity.magnitude);
//      // Update animation parameters
//      _animator.SetBool("move", shouldMove);
//      _animator.SetFloat ("velx", _velocity.x);
//      _animator.SetFloat ("vely", _velocity.y);

      //GetComponent<LookAt>().lookAtTargetPosition = _agent.steeringTarget + transform.forward;
   }

   private void OnAnimatorMove ()
   {
      // Update position to agent position
      transform.position = _navMeshAgent.nextPosition;
   }
}
