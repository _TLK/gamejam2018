﻿using UnityEngine;

public class SpawnImaginaryCharacterOnAltFire : MonoBehaviour
{
  public Character Imaginary;
  public Transform Boss;
  public float MaxAllowedDistanceToBoss = 5;

  void Update()
  {
    if (Input.GetButtonDown ("Fire2"))
    {
      var distance = (this.transform.position - Boss.position).magnitude;
      if (distance < MaxAllowedDistanceToBoss)
        this.SpawnBuddy ();
    }
  }

  public void SpawnBuddy ()
  {
    Vector3 invertedPosition = Boss.position - (this.transform.position - Boss.position);
    var targetPosition = new Vector3 (
      invertedPosition.x,
      this.transform.position.y,
      invertedPosition.z
    );
    Imaginary.transform.position =
      targetPosition;
    Imaginary.transform.rotation = Quaternion.Euler (-this.transform.rotation.eulerAngles);
    Imaginary.gameObject.SetActive (true);
    //this.enabled = false;
  }
}