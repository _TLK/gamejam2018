﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCharacterOnAltFire : MonoBehaviour
{
  public Character Other;

  void Update()
  {
    if (Input.GetButtonDown ("Fire2"))
    {
      this.Switch ();
    }
  }

  public void Switch ()
  {
    Other.transform.position = this.transform.position;
    Other.transform.rotation = this.transform.rotation;
    Other.gameObject.SetActive (true);
    this.gameObject.SetActive (false);
  }
}