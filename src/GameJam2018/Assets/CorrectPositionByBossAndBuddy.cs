﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrectPositionByBossAndBuddy : MonoBehaviour
{
  public Transform Buddy;
  public Transform Boss;
  public float Speed = 6;
  CharacterController _controller;

  void Update()
  {
    CharacterController controller = _controller ?? (_controller = GetComponent<CharacterController>());

    Vector3 invertedPosition =
      Boss.position - 0.5f * (Buddy.position - Boss.position);
    var targetPosition = new Vector3(
      invertedPosition.x,
      this.transform.position.y,
      invertedPosition.z
    );

    var direction = (targetPosition - this.transform.position).normalized;

    controller.Move (Speed * direction * Time.deltaTime);
  }
}
